package Appium;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.net.MalformedURLException;


/**
 * Created by USer on 11/12/2016.
 */
public class Driver {
    private static int _waitTimeout = 20;

    public AppiumDriver<MobileElement> AppiumDriver;
    public WebDriverWait Wait;
    private static String appPathAndroid = "/Users/victor.vargas/Desktop/odigeo-repository/android-native-app/eDreams/build/outputs/apk/eDreams-debug.apk";
    private static String appPathIOS = "/Users/victor.vargas/Desktop/Victor/ipas/eDreams.ipa";
    private Server appiumServer = new Server();

    public void InitializeForAndroid(String platformVersion, String browserName, String deviceName) throws MalformedURLException {
        startServer();
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability(MobileCapabilityType.APP, appPathAndroid);
        capabilities.setCapability(MobileCapabilityType.VERSION, platformVersion);
        capabilities.setCapability(MobileCapabilityType.BROWSER_NAME, browserName);
        capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, deviceName);
        AppiumDriver = new AndroidDriver<>(appiumServer.getService(), capabilities);
        Wait = new WebDriverWait(AppiumDriver, _waitTimeout);
    }

    public void InitializeForiOS(String platformVersion, String browserName, String deviceName, String udid) throws MalformedURLException {
        startServer();
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("autoAcceptAlerts", true); //Accept allways permissions when requested
        capabilities.setCapability(MobileCapabilityType.APP, appPathIOS);
        capabilities.setCapability(MobileCapabilityType.VERSION, platformVersion);
        capabilities.setCapability(MobileCapabilityType.BROWSER_NAME, browserName);
        capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, deviceName);
        capabilities.setCapability(MobileCapabilityType.UDID, udid);
        capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME,"XCUITest");
        capabilities.setCapability("fullReset",true);
        AppiumDriver = new IOSDriver<>(appiumServer.getService(), capabilities);

        Wait = new WebDriverWait(AppiumDriver, _waitTimeout);
    }

    private void startServer(){
        appiumServer.start();
    }

    public void tearDown() {
        AppiumDriver.quit();
        appiumServer.stop();
    }
}

