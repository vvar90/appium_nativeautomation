package Appium;

import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;

import java.io.File;

/**
 * Created by victor.vargas on 04/01/2017.
 */
public class Server {

    private AppiumDriverLocalService service;
    private static File nodeJS = new File("/usr/local/bin/node");
    private static File appiumJS = new File("/Users/victor.vargas/appium/build/lib/main.js");

    public void start(){
        initializeAppiumDriverLocalService();
        service.start();
    }

    private void initializeAppiumDriverLocalService(){
        service = AppiumDriverLocalService.buildService(initializeAppiumServiceBuilder());

    }
    private AppiumServiceBuilder initializeAppiumServiceBuilder(){
        return new AppiumServiceBuilder()
                .usingDriverExecutable(nodeJS)
                .withAppiumJS(appiumJS)
                .withIPAddress("127.0.0.1")
                .usingPort(4723);
                //.usingAnyFreePort();
    }

    public void stop(){
        service.stop();
    }

    public AppiumDriverLocalService getService(){
        return this.service;
    }

}
