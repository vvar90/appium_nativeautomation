package ConfigsManager;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.Properties;

/**
 * Created by victor.vargas on 04/01/2017.
 */
public class ConfigsManager {
    InputStream inputStream;

    public String getConfig(String key) throws IOException {

            String value;
            Properties prop = new Properties();
            String propFileName = "config.properties";

            inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);

            if (inputStream != null) {
                prop.load(inputStream);
            } else {
                throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
            }
            value = prop.getProperty(key);
            inputStream.close();
            return value;
        }
    }

