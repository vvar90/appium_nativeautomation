package PageObjects.Components;

import Appium.Driver;
import PageObjects.Pages.BasePageObject;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.iOSFindBy;

/**
 * Created by victor.vargas on 02/01/2017.
 */
public class PermissionsDialog extends BasePageObject {

    public PermissionsDialog(Driver driver){
        Factory.GetPermissionDialog(this, driver);
    }

    //Locators
    @iOSFindBy(xpath = "//XCUIElementTypeOther[2]/XCUIElementTypeAlert/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[3]/XCUIElementTypeButton")
    private MobileElement _permissionAllowed;

    @iOSFindBy(xpath = "//XCUIElementTypeOther[2]/XCUIElementTypeAlert/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeButton")
    private MobileElement _permissionDeny;

    public void allowPermission(){
        _permissionAllowed.click();
    }
}
