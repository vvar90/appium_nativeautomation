package PageObjects.Pages;

import Appium.Driver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSFindBy;
import org.openqa.selenium.By;

/**
 * Created by USer on 18/12/2016.
 */
public class HomePage extends BasePageObject{

    private static By androidBy = By.id("rlFlight");
    private static By iOSBy = By.xpath("//XCUIElementTypeApplication/XCUIElementTypeWindow[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[2]/XCUIElementTypeStaticText");
    private By by;

    public HomePage(Driver driver, String os){
        if (os.equals("iOS")){
            by = iOSBy;
        }
        else{
            by = androidBy;
        }
        Factory.GetHomePageObject(this, driver, by);
    }

    //Locators
    @AndroidFindBy(id = "rlFlight")
    @iOSFindBy(xpath = "//XCUIElementTypeApplication/XCUIElementTypeWindow[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[2]/XCUIElementTypeStaticText")
    private MobileElement _flights;

    @AndroidFindBy(id = "rlHotel")
    @iOSFindBy(xpath = "//XCUIElementTypeApplication/XCUIElementTypeWindow[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[3]/XCUIElementTypeStaticText")
    private MobileElement _hotels;

    @AndroidFindBy(id = "rlCar")
    @iOSFindBy(xpath = "///XCUIElementTypeApplication/XCUIElementTypeWindow[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[4]/XCUIElementTypeStaticText")
    private MobileElement _cars;

    //Methods
    public void goToFlights(){
        _flights.click();
    }

    public boolean isAt(){
        return (_flights.isDisplayed() && _hotels.isDisplayed() && _cars.isDisplayed());
    }
}
