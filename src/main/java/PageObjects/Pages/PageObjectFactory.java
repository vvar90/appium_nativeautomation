package PageObjects.Pages;

import Appium.Driver;
import PageObjects.Components.PermissionsDialog;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

/**
 * Created by USer on 11/12/2016.
 */
public class PageObjectFactory {

    public HomePage GetHomePageObject (HomePage page, Driver driver, By element){
        driver.Wait.until(ExpectedConditions.visibilityOfElementLocated(element));
        PageFactory.initElements(new AppiumFieldDecorator(driver.AppiumDriver), page);
        return page;
    }


    public WalkthroughPage GetWalkthroughPageObject(WalkthroughPage page, Driver driver, By element) {
        driver.Wait.until(ExpectedConditions.visibilityOfElementLocated(element));
        PageFactory.initElements(new AppiumFieldDecorator(driver.AppiumDriver), page);
        return page;
    }

    public SearchPage GetSearchPageObject(SearchPage page, Driver driver, boolean isPopUpDisplayed) {
        if(isPopUpDisplayed){
            driver.Wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("dialog_container")));
        }
        else {
            driver.Wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("android.support.v7.widget.LinearLayoutCompat[1]/android.support.v7.app.ActionBar.Tab[2]")));
        }
        PageFactory.initElements(new AppiumFieldDecorator(driver.AppiumDriver), page);
        return page;
    }

    public PermissionsDialog GetPermissionDialog(PermissionsDialog dialog, Driver driver) {
        driver.Wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//XCUIElementTypeApplication/XCUIElementTypeWindow/XCUIElementTypeOther[2]/XCUIElementTypeAlert/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[3]")));
        PageFactory.initElements(new AppiumFieldDecorator(driver.AppiumDriver), dialog);
        return dialog;
    }
}
