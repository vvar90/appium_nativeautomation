package PageObjects.Pages;

import Appium.Driver;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.By;

/**
 * Created by victor.vargas on 02/01/2017.
 */
public class SearchPage extends BasePageObject {

    private AppiumDriver<MobileElement> _appiumDriver;

    public SearchPage(Driver driver, String os) {
        _appiumDriver = driver.AppiumDriver;
        if(isPermissionPopUpPresent()) { //Allow permision and load page again
            Factory.GetSearchPageObject(this, driver, true);
            _popUpAllow.click();
        }
            Factory.GetSearchPageObject(this, driver, false);
    }

    //Locators
    @AndroidFindBy(id = "permission_allow_button")
    //@iOSFindBy(id="")
    private MobileElement _popUpAllow;

    @AndroidFindBy(id = "permission_deny_button")
    //@iOSFindBy(id="")
    private MobileElement _popUpDeny;

    @AndroidFindBy(xpath = "android.support.v7.widget.LinearLayoutCompat[1]/android.support.v7.app.ActionBar.Tab[2]")
    //@iOSFindBy(id="")
    private MobileElement _oneWay;

    @AndroidFindBy(xpath = "android.support.v7.widget.LinearLayoutCompat[1]/android.support.v7.app.ActionBar.Tab[1]")
    //@iOSFindBy(id="")
    private MobileElement _roundTrip;

    @AndroidFindBy(xpath = "android.support.v7.widget.LinearLayoutCompat[1]/android.support.v7.app.ActionBar.Tab[3]")
    //@iOSFindBy(id="")
    private MobileElement _multiStop;

    //Properties
    public String roundTripText(){
        return _roundTrip.getText();
    }

    //Methods
    public boolean isAt(){
        return (_oneWay.isDisplayed() && _roundTrip.isDisplayed() && _multiStop.isDisplayed());
    }

    private boolean isPermissionPopUpPresent(){
        return (_appiumDriver.findElements(By.xpath("//*[@class='android.widget.Button'][2]")).size()>0);
    }
}
