package PageObjects.Pages;

import Appium.Driver;
import PageObjects.Components.PermissionsDialog;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSFindBy;
import org.openqa.selenium.By;

/**
 * Created by victor.vargas on 29/12/2016.
 */
public class WalkthroughPage extends BasePageObject {

    private static By androidby = By.id("menu_item_skip_walkthrough");
    private static By iOSBy = By.xpath("//XCUIElementTypeApplication/XCUIElementTypeWindow/XCUIElementTypeOther/XCUIElementTypeNavigationBar/XCUIElementTypeButton");
    private By by;
    private PermissionsDialog dialog;

    public WalkthroughPage(Driver driver, String os) throws InterruptedException {
        //Only for iOS, remove persuasive message + permission popup
        if (os.equals("iOS")){
            by = iOSBy;
            allowPersuasiveAndPermissionDialogs(driver);
        }
        else {
            by = androidby;
        }
        Factory.GetWalkthroughPageObject(this, driver, by);
    }

    //Locators
    @AndroidFindBy(id = "menu_item_skip_walkthrough")
    @iOSFindBy(xpath = "//XCUIElementTypeApplication/XCUIElementTypeWindow[2]/XCUIElementTypeOther/XCUIElementTypeNavigationBar/XCUIElementTypeButton")
    private MobileElement _doneButton;

    //Methods
    public void skipWalkthrough(){
        _doneButton.click();
    }

    public void setPermissions(){
        dialog.allowPermission();
    }

    private void allowPersuasiveAndPermissionDialogs(Driver driver) throws InterruptedException {
        dialog = new PermissionsDialog(driver);
        setPermissions();
        Thread.sleep(500);   //To reduce flakyness
        dialog = new PermissionsDialog(driver);
        setPermissions();
        Thread.sleep(500);   //To reduce flakyness
    }
}
