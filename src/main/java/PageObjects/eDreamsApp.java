package PageObjects;

import Appium.Driver;
import PageObjects.Pages.HomePage;
import PageObjects.Pages.SearchPage;
import PageObjects.Pages.WalkthroughPage;

/**
 * Created by USer on 18/12/2016.
 */
public class eDreamsApp {

    private Driver _driver;
    private String _os;

    public eDreamsApp(Driver driver, String os){
        _driver = driver;
        _os = os;
    }

    //Pages
    public HomePage home() {return new HomePage(_driver, _os);}
    public WalkthroughPage walkthrough() throws InterruptedException {return new WalkthroughPage(_driver, _os);}
    public SearchPage searchPage() {return new SearchPage(_driver, _os);}
}
