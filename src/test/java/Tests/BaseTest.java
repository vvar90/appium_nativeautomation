package Tests;

import ConfigsManager.ConfigsManager;
import Appium.Driver;
import PageObjects.eDreamsApp;
import org.junit.After;
import org.junit.Before;

import java.io.IOException;

/**
 * Created by USer on 18/12/2016.
 */
public class BaseTest {

    protected eDreamsApp eDreams;
    protected Driver driver;
    public String _os;
    private ConfigsManager _configsManager = new ConfigsManager();

    @Before
    public void Initialize() throws IOException {
        _os = _configsManager.getConfig("os");

        //Not working, comparation is done incorrectly...
        this.driver = new Driver();
        if(_os.equals("Android")){
            driver.InitializeForAndroid("6.0.1", "Android", "0441bb718291f8e4");
        }
        else if (_os.equals("iOS")) {
            driver.InitializeForiOS("10.1.1", "iOS", "iPhone 6 Plus", "474ad35c5df104edd42d992fe87c206e78483796");
        }
        this.eDreams = new eDreamsApp(this.driver, _os);
    }

    @After
    public void TearDown(){
        driver.tearDown();
    }



}
