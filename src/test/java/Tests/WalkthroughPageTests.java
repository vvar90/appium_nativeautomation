package Tests;

import PageObjects.eDreamsApp;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by USer on 18/12/2016.
 */
public class WalkthroughPageTests extends BaseTest{

    @Test
    public void ShouldGoToHomePage() throws InterruptedException {
        //Arrange

        //Act
        eDreams.walkthrough().skipWalkthrough();

        //Assert
        Assert.assertTrue("This is not the Home Page", eDreams.home().isAt());
    }
}
